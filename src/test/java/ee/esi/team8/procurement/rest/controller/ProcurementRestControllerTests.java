package ee.esi.team8.procurement.rest.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import ee.esi.team8.ProcurementApplication;
import ee.esi.team8.common.application.dto.BusinessPeriodDTO;
import ee.esi.team8.common.domain.model.BusinessPeriod;
import ee.esi.team8.hr.domain.model.EmployeeID;
import ee.esi.team8.procurement.application.dto.CommentDTO;
import ee.esi.team8.procurement.application.dto.CreatePHRDTO;
import ee.esi.team8.procurement.application.dto.ExtendPHRDTO;
import ee.esi.team8.procurement.application.dto.PlantHireRequestDTO;
import ee.esi.team8.procurement.application.service.CommentAssembler;
import ee.esi.team8.procurement.application.service.PlantHireRequestAssembler;
import ee.esi.team8.procurement.application.service.ProcurementService;
import ee.esi.team8.procurement.domain.model.Comment;
import ee.esi.team8.procurement.domain.model.PHRStatus;
import ee.esi.team8.procurement.domain.model.PlantHireRequest;
import ee.esi.team8.procurement.repo.CommentRepository;
import ee.esi.team8.procurement.repo.PlantHireRequestRepository;
import ee.esi.team8.rental.application.dto.PlantInventoryEntryDTO;
import ee.esi.team8.rental.application.dto.PurchaseOrderDTO;
import ee.esi.team8.rental.application.services.RentalService;
import ee.esi.team8.rental.domain.model.POStatus;
import ee.esi.team8.rental.domain.model.PlantInventoryEntry;
import ee.esi.team8.rental.domain.model.PurchaseOrder;
import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.transaction.Transactional;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.text.IsEmptyString.isEmptyOrNullString;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {ProcurementApplication.class,
        ProcurementRestControllerTests.RentalServiceMock.class})
@WebAppConfiguration
@Transactional
public class ProcurementRestControllerTests {
    @Autowired
    private WebApplicationContext wac;
    private MockMvc mockMvc;

    @Autowired
    @Qualifier("_halObjectMapper")
    ObjectMapper mapper;

    @Autowired
    RentalService rentalService;

    @Autowired
    ProcurementService procurementService;

    @Autowired
    PlantHireRequestAssembler plantHireRequestAssembler;

    @Autowired
    CommentAssembler commentAssembler;

    @Autowired
    PlantHireRequestRepository plantHireRequestRepo;

    @Autowired
    CommentRepository commentRepo;

    @Configuration
    static class RentalServiceMock {
        @Bean
        public RentalService rentalService() {
            return Mockito.mock(RentalService.class);
        }
    }

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    @After
    public void tearOff() {
        plantHireRequestRepo.deleteAll();
    }

    @Test
    public void testGetAllPlants() throws Exception {
        Resource responseBody = new ClassPathResource("trucks.json", this.getClass());
        List<PlantInventoryEntryDTO> list =
                mapper.readValue(responseBody.getFile(), new TypeReference<List<PlantInventoryEntryDTO>>() {
                });
        LocalDate startDate = LocalDate.now();
        LocalDate endDate = startDate.plusDays(2);
        when(rentalService.findAvailablePlants("Truck", startDate, endDate)).thenReturn(list);
        MvcResult result = mockMvc.perform(
                get("/api/plants?name=Truck&startDate={start}&endDate={end}", startDate, endDate))
                .andExpect(status().isOk())
                .andReturn();

        assertEquals(
                list,
                mapper.readValue(result.getResponse().getContentAsString(),
                        new TypeReference<List<PlantInventoryEntryDTO>>() {
                        }));
    }

    @Test
    @Sql("requests-dataset.sql")
    public void testGetAllPlantHireRequests() throws Exception {
        MvcResult result = mockMvc.perform(
                get("/api/requests"))
                .andExpect(status().isOk())
                .andExpect(header().string("Location", isEmptyOrNullString()))
                .andReturn();

        List<PlantHireRequestDTO> requests = mapper.readValue(
                result.getResponse().getContentAsString(),
                new TypeReference<List<PlantHireRequestDTO>>() {
                });

        assertThat(requests.size()).isEqualTo(6);
    }

    @Test
    @Sql("requests-dataset.sql")
    public void testGetAllPlantHireRequestsWithStatus() throws Exception {
        //PHRs with status HIRED
        MvcResult hiredResult = mockMvc.perform(
                get("/api/requests?status=HIRED"))
                .andExpect(status().isOk())
                .andExpect(header().string("Location", isEmptyOrNullString()))
                .andReturn();

        List<PlantHireRequestDTO> hiredRequests = mapper.readValue(
                hiredResult.getResponse().getContentAsString(),
                new TypeReference<List<PlantHireRequestDTO>>() {
                });

        assertThat(hiredRequests.size()).isEqualTo(1);
        assertThat(hiredRequests.get(0).get_id()).isEqualTo("3");

        //PHRs with status PENDING
        MvcResult pendingResult = mockMvc.perform(
                get("/api/requests?status=PENDING"))
                .andExpect(status().isOk())
                .andExpect(header().string("Location", isEmptyOrNullString()))
                .andReturn();

        List<PlantHireRequestDTO> pendingRequests = mapper.readValue(
                pendingResult.getResponse().getContentAsString(),
                new TypeReference<List<PlantHireRequestDTO>>() {
                });

        assertThat(pendingRequests.size()).isEqualTo(2);
        //TODO: Consider that arbitary list implementation might switch the order of these objects
        assertThat(pendingRequests.get(0).get_id()).isEqualTo("2");
        assertThat(pendingRequests.get(1).get_id()).isEqualTo("6");

//        //PHRs with status HIRED or PENDING
//        MvcResult hiredOrPendingResult = mockMvc.perform(
//                get("/api/requests?status[]=PENDING&status[]=HIRED"))
//                .andExpect(status().isOk())
//                .andExpect(header().string("Location", isEmptyOrNullString()))
//                .andReturn();
//
//        List<PlantHireRequestDTO> hiredOrPendingRequests = mapper.readValue(
//                hiredOrPendingResult.getResponse().getContentAsString(),
//                new TypeReference<List<PlantHireRequestDTO>>() {
//                });
//
//        assertThat(hiredOrPendingRequests.size()).isEqualTo(3);
//        //TODO: Consider that arbitary list implementation might switch the order of these objects
//        assertThat(hiredOrPendingRequests.get(0).get_id()).isEqualTo("2");
//        assertThat(hiredOrPendingRequests.get(1).get_id()).isEqualTo("6");
//        assertThat(hiredOrPendingRequests.get(1).get_id()).isEqualTo("3");


        //PHRs with status ACCEPTED
        MvcResult acceptedResult = mockMvc.perform(
                get("/api/requests?status=ACCEPTED"))
                .andExpect(status().isOk())
                .andExpect(header().string("Location", isEmptyOrNullString()))
                .andReturn();

        List<PlantHireRequestDTO> acceptedRequests = mapper.readValue(
                acceptedResult.getResponse().getContentAsString(),
                new TypeReference<List<PlantHireRequestDTO>>() {
                });

        assertThat(acceptedRequests.size()).isEqualTo(1);
        assertThat(acceptedRequests.get(0).get_id()).isEqualTo("5");

        //PHRs with status REJECTED
        MvcResult rejectedResult = mockMvc.perform(
                get("/api/requests?status=REJECTED"))
                .andExpect(status().isOk())
                .andExpect(header().string("Location", isEmptyOrNullString()))
                .andReturn();

        List<PlantHireRequestDTO> rejectedRequests = mapper.readValue(
                rejectedResult.getResponse().getContentAsString(),
                new TypeReference<List<PlantHireRequestDTO>>() {
                });

        assertThat(rejectedRequests.size()).isEqualTo(1);
        assertThat(rejectedRequests.get(0).get_id()).isEqualTo("4");

        //PHRs with status CLOSED
        MvcResult closedResult = mockMvc.perform(
                get("/api/requests?status=CLOSED"))
                .andExpect(status().isOk())
                .andExpect(header().string("Location", isEmptyOrNullString()))
                .andReturn();

        List<PlantHireRequestDTO> closedRequests = mapper.readValue(
                closedResult.getResponse().getContentAsString(),
                new TypeReference<List<PlantHireRequestDTO>>() {
                });

        assertThat(closedRequests.size()).isEqualTo(1);
        assertThat(closedRequests.get(0).get_id()).isEqualTo("1");
    }


    @Test
    @Sql("requests-dataset.sql")
    public void testPHRFetchingWithPurchaseOrder() throws Exception {
        PlantHireRequest phr = plantHireRequestRepo.getOne("3");
        String poId = "74eeb615-2e85-4586-9673-8df91be1e1c9";
        PurchaseOrder po = PurchaseOrder.of(poId, new BigDecimal(100));
        phr.updatePurcaseOrder(po);
        plantHireRequestRepo.save(phr);

        PlantHireRequest fetchedPHR = plantHireRequestRepo.findByPurchaseOrderId(poId);

        assertEquals(fetchedPHR.getId(), phr.getId());
    }

    @Test
    public void testPurchaseOrderCreation() throws Exception {
        Resource responseBody = new ClassPathResource("trucks.json", this.getClass());
        List<PlantInventoryEntryDTO> plantInventoryEntryDTOs =
                mapper.readValue(responseBody.getFile(), new TypeReference<List<PlantInventoryEntryDTO>>() {
                });

        BusinessPeriodDTO rentalPeriod = BusinessPeriodDTO.of(LocalDate.now(), LocalDate.now().plusDays(2));

        PlantInventoryEntryDTO firstEntry = plantInventoryEntryDTOs.get(0);
        PlantInventoryEntry plant = PlantInventoryEntry.of(firstEntry.get_id(), firstEntry.getName(), firstEntry.getLinks().get(0).getHref());

        ResponseEntity<PurchaseOrderDTO> response = new ResponseEntity<PurchaseOrderDTO>(
                PurchaseOrderDTO.of(plant, rentalPeriod),
                HttpStatus.CREATED);
        response.getBody().add(new Link("hello"));
        response.getBody().setStatus(POStatus.OPEN);

        when(rentalService.createPurchaseOrder(any())).thenReturn(response);

        EmployeeID siteEngineer = EmployeeID.of("siteEngineerRef");
        String constructionSiteId = "1";
        CreatePHRDTO createPHRDTO = CreatePHRDTO.of(rentalPeriod, firstEntry, constructionSiteId);
        PlantHireRequestDTO hireRequestDTO = procurementService.createPlantHireRequest(createPHRDTO, siteEngineer);

        MvcResult result = mockMvc.perform(
                post("/api/orders")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(hireRequestDTO)))
                .andExpect(status().isCreated())
                .andReturn();

        assertEquals(response.getBody(), mapper.readValue(result.getResponse().getContentAsString(),
                new TypeReference<PurchaseOrderDTO>() {
                }));
    }

    @Test
    public void testResubmitPurchaseOrder() throws Exception {
        Resource responseBody = new ClassPathResource("trucks.json", this.getClass());
        List<PlantInventoryEntryDTO> plantInventoryEntryDTOs =
                mapper.readValue(responseBody.getFile(), new TypeReference<List<PlantInventoryEntryDTO>>() {
                });

        BusinessPeriod rentalPeriod = BusinessPeriod.of(LocalDate.now(), LocalDate.now().plusDays(2));

        PlantInventoryEntryDTO firstEntry = plantInventoryEntryDTOs.get(0);
        PlantInventoryEntry plant = PlantInventoryEntry.of(firstEntry.get_id(), firstEntry.getName(), firstEntry.getLinks().get(0).getHref());

        ResponseEntity<PurchaseOrderDTO> response = new ResponseEntity<PurchaseOrderDTO>(
                PurchaseOrderDTO.of(plant, BusinessPeriodDTO.of(rentalPeriod.getStartDate(), rentalPeriod.getEndDate())),
                HttpStatus.OK);

        when(rentalService.resubmitPurchaseOrder(response.getBody())).thenReturn(response);

        MvcResult result = mockMvc.perform(
                put("/api/orders")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(response.getBody())))
                .andExpect(status().isOk())
                .andReturn();

        assertEquals(response.getBody(), mapper.readValue(result.getResponse().getContentAsString(),
                new TypeReference<PurchaseOrderDTO>() {
                }));
    }

    @Test
    public void testCancelPurchaseOrder() throws Exception {
        Resource responseBody = new ClassPathResource("trucks.json", this.getClass());
        List<PlantInventoryEntryDTO> plantInventoryEntryDTOs =
                mapper.readValue(responseBody.getFile(), new TypeReference<List<PlantInventoryEntryDTO>>() {
                });

        BusinessPeriodDTO rentalPeriod = BusinessPeriodDTO.of(LocalDate.now(), LocalDate.now().plusDays(2));

        PlantInventoryEntryDTO firstEntry = plantInventoryEntryDTOs.get(0);
        PlantInventoryEntry plant = PlantInventoryEntry.of(firstEntry.get_id(), firstEntry.getName(), firstEntry.getLinks().get(0).getHref());

        PurchaseOrderDTO dto = PurchaseOrderDTO.of(plant, rentalPeriod);
        dto.set_id("1");

        //TODO: The test should work like this, needs refactoring of cancelPurchaseOrder
//        EmployeeID siteEngineer = EmployeeID.of("siteEngineerRef");
//        EmployeeID worksEngineer = EmployeeID.of("worksEngineerRef");
//        String constructionSiteId = "1";
//        CreatePHRDTO createPHRDTO = CreatePHRDTO.of(rentalPeriod, plant, constructionSiteId);
//        PlantHireRequestDTO hireRequestDTO = procurementService.createPlantHireRequest(createPHRDTO, siteEngineer);
//        PlantHireRequest phr = procurementService.acceptPlantHireRequest(hireRequestDTO.get_id(), worksEngineer);
//        PurchaseOrder po = phr.getPurchaseOrder();

        ResponseEntity<PurchaseOrderDTO> response = new ResponseEntity<PurchaseOrderDTO>(dto, HttpStatus.ACCEPTED);

        when(rentalService.cancelPurchaseOrder(response.getBody().get_id())).thenReturn(response);

        MvcResult result = mockMvc.perform(
                delete("/api/orders/" + response.getBody().get_id()))
                .andExpect(status().isAccepted())
                .andReturn();

        assertEquals(response.getBody(), mapper.readValue(result.getResponse().getContentAsString(),
                new TypeReference<PurchaseOrderDTO>() {
                }));
    }

    @Test
    public void testPlantHireRequestCreationAndUpdating() throws Exception {
        //PHR CREATION
        // Setup request data
        Resource responseBody = new ClassPathResource("trucks.json", this.getClass());
        List<PlantInventoryEntryDTO> plantInventoryEntryDTOs =
                mapper.readValue(responseBody.getFile(), new TypeReference<List<PlantInventoryEntryDTO>>() {
                });

        BusinessPeriodDTO rentalPeriod = BusinessPeriodDTO.of(LocalDate.now(), LocalDate.now().plusDays(2));

        PlantInventoryEntryDTO firstEntry = plantInventoryEntryDTOs.get(0);
        PlantInventoryEntry plant = PlantInventoryEntry.of(firstEntry.get_id(), firstEntry.getName(), firstEntry.getLinks().get(0).getHref());

        String constructionSiteId = "1";

        CreatePHRDTO phrDTO = CreatePHRDTO.of(rentalPeriod, firstEntry, constructionSiteId);

        // Call to controller
        MvcResult result = mockMvc.perform(
                post("/api/requests")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(phrDTO)))
                .andExpect(status().isCreated())
                .andReturn();


        PlantHireRequestDTO phrResult = mapper.readValue(result.getResponse().getContentAsString(), PlantHireRequestDTO.class);

        assertEquals(BusinessPeriodDTO.of(rentalPeriod.getStartDate(), rentalPeriod.getEndDate()), phrResult.getHirePeriod());
        assertEquals(PHRStatus.PENDING, phrResult.getStatus());
        assertEquals(plant, phrResult.getPlant());
        assertEquals(constructionSiteId, phrResult.getConstructionSiteId());
        assertEquals(EmployeeID.of("siteEngineerRef"), phrResult.getSiteEngineer());
        assertEquals(rentalPeriod, phrResult.getHirePeriod());

        //CREATED PHR UPDATING
        rentalPeriod = BusinessPeriodDTO.of(LocalDate.now().plusDays(3), LocalDate.now().plusDays(5));
        PlantInventoryEntryDTO secondEntry = plantInventoryEntryDTOs.get(1);
        plant = PlantInventoryEntry.of(secondEntry.get_id(), secondEntry.getName(), secondEntry.getLinks().get(0).getHref());
        constructionSiteId = "2";

        phrDTO = CreatePHRDTO.of(rentalPeriod, secondEntry, constructionSiteId);

        // Call to controller
        result = mockMvc.perform(
                patch("/api/requests/" + phrResult.get_id())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(phrDTO)))
                .andExpect(status().isCreated())
                .andReturn();

        phrResult = mapper.readValue(result.getResponse().getContentAsString(), PlantHireRequestDTO.class);

        assertEquals(BusinessPeriodDTO.of(rentalPeriod.getStartDate(), rentalPeriod.getEndDate()), phrResult.getHirePeriod());
        assertEquals(PHRStatus.PENDING, phrResult.getStatus());
        assertEquals(plant, phrResult.getPlant());
        assertEquals(constructionSiteId, phrResult.getConstructionSiteId());
        assertEquals(rentalPeriod, phrResult.getHirePeriod());
        assertEquals(EmployeeID.of("siteEngineerRef"), phrResult.getSiteEngineer());

    }

    @Test
    @Sql("requests-dataset.sql")
    public void testPlantHireRequestAcceptance() throws Exception {
        // Mock setup
        ResponseEntity<PurchaseOrderDTO> response = mockPO();
        when(rentalService.createPurchaseOrderAndReturnBody(any())).thenReturn(response.getBody());

        // Call to controller
        mockMvc.perform(
                post("/api/requests/2/accept")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString("")))
                .andExpect(status().isOk())
                .andReturn();

        PlantHireRequest phr = plantHireRequestRepo.getOne("2");

        assertEquals(PHRStatus.HIRED, phr.getStatus());
        assertEquals(BigDecimal.valueOf(30), phr.getPurchaseOrder().getTotal());
        assertEquals("hello", phr.getPurchaseOrder().getHref());
        assertEquals(EmployeeID.of("worksEngineerRef"), phr.getWorksEngineer());

    }

    @Test
    @Sql("requests-dataset.sql")
    public void testPlantHireRequestRejection() throws Exception {
        EmployeeID worksEngineer = EmployeeID.of("Reference to works engineer");
        PlantHireRequest phr = plantHireRequestRepo.findOne("2");
        CommentDTO commentDTO = commentAssembler.toResource(Comment.of(worksEngineer,"No comments",phr));
        MvcResult result = mockMvc.perform(
                post("/api/requests/2/reject")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(commentDTO)))
                .andExpect(status().isOk())
                .andReturn();
        assertEquals(1,commentRepo.findAll().size());
        List<Comment> comments = phr.getComments();
        assertEquals(1,comments.size());
        assertEquals("Rejected",phr.getStatus().toString());
        assertEquals("No comments",phr.getComments().get(0).getText());
    }

    @Test
    @Sql("requests-dataset.sql")
    public void testPlantHireRequestCancellation() throws Exception {
        EmployeeID worksEngineer = EmployeeID.of("Reference to works engineer");
        PlantHireRequest phr = plantHireRequestRepo.findOne("5");
        MvcResult result = mockMvc.perform(
                post("/api/requests/5/cancel")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString("")))
                .andExpect(status().isOk())
                .andReturn();
        assertEquals("Cancelled",phr.getStatus().toString());
    }

    @Test
    @Sql("requests-dataset.sql")
    public void testPlantHireRequestExtensionRequest() throws Exception {

        //Invalid extension request: the PHR is not hired yet - extension unavailable
        PlantHireRequest invalidPHR = plantHireRequestRepo.findOne("2");
        ExtendPHRDTO invalidExtendPHRDTO1 = ExtendPHRDTO.of(LocalDate.of(2016, 3, 25));
        mockMvc.perform(
                patch("/api/requests/2/extend")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(invalidExtendPHRDTO1)))
                .andExpect(status().isOk())
                .andReturn();
        assertEquals(null, invalidPHR.getExtensionEndDate());


        //Invalid extension request: extension date before current rental end date
        PlantHireRequest validPHR = plantHireRequestRepo.findOne("3");
        ExtendPHRDTO invalidExtendPHRDTO2 = ExtendPHRDTO.of(LocalDate.of(2016, 3, 25));
        mockMvc.perform(
                patch("/api/requests/3/extend")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(invalidExtendPHRDTO2)))
                .andExpect(status().isOk())
                .andReturn();
        assertEquals(null, validPHR.getExtensionEndDate());

        //Valid extension request: extension date in the future and plant is hired
        ExtendPHRDTO validExtendPHRDTO = ExtendPHRDTO.of(LocalDate.of(2017, 3, 25));
        mockMvc.perform(
                patch("/api/requests/3/extend")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(validExtendPHRDTO)))
                .andExpect(status().isOk())
                .andReturn();
        assertEquals(validExtendPHRDTO.getExtensionEndDate(), validPHR.getExtensionEndDate());
    }

    @Test
    @Sql("requests-dataset.sql")
    public void testPlantHireRequestExtensionAcceptance() throws Exception {
        //Create a valid PHR request
        PlantHireRequest validPHR = plantHireRequestRepo.findOne("3");
        ExtendPHRDTO validExtendPHRDTO = ExtendPHRDTO.of(LocalDate.of(2017, 3, 25));
        mockMvc.perform(
                patch("/api/requests/3/extend")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(validExtendPHRDTO)))
                .andExpect(status().isOk())
                .andReturn();
        assertEquals(validExtendPHRDTO.getExtensionEndDate(), validPHR.getExtensionEndDate());

        PurchaseOrder po = validPHR.getPurchaseOrder();
        when(rentalService.getPurchaseOrderId(po)).thenReturn("420");
        String poid = rentalService.getPurchaseOrderId(po);

        //Mock receiving acceptance from the RentIt
        mockMvc.perform(
                post("/api/orders/" + poid + "/acceptExtension")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString("")))
                .andExpect(status().isOk())
                .andReturn();

        assertEquals(null, validPHR.getExtensionEndDate());
        assertEquals(validExtendPHRDTO.getExtensionEndDate(), validPHR.getRentalPeriod().getEndDate());
    }

    @Test
    @Sql("requests-dataset.sql")
    public void testPlantHireRequestExtensionRejection() throws Exception {
        //Create a valid PHR request
        PlantHireRequest validPHR = plantHireRequestRepo.findOne("3");
        LocalDate oldEndDate = validPHR.getRentalPeriod().getEndDate();
        ExtendPHRDTO validExtendPHRDTO = ExtendPHRDTO.of(LocalDate.of(2017, 3, 25));
        mockMvc.perform(
                patch("/api/requests/3/extend")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(validExtendPHRDTO)))
                .andExpect(status().isOk())
                .andReturn();
        assertEquals(validExtendPHRDTO.getExtensionEndDate(), validPHR.getExtensionEndDate());

        PurchaseOrder po = validPHR.getPurchaseOrder();
        when(rentalService.getPurchaseOrderId(po)).thenReturn("420");
        String poid = rentalService.getPurchaseOrderId(po);

        //Mock receiving rejection from the RentIt
        mockMvc.perform(
                delete("/api/orders/" + poid + "/rejectExtension")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString("")))
                .andExpect(status().isOk())
                .andReturn();

        assertEquals(null, validPHR.getExtensionEndDate());
        assertEquals(oldEndDate, validPHR.getRentalPeriod().getEndDate());
    }


    private ResponseEntity<PurchaseOrderDTO> mockPO() throws IOException {
        // Setup request data
        Resource responseBody = new ClassPathResource("trucks.json", this.getClass());
        List<PlantInventoryEntryDTO> plantInventoryEntryDTOs =
                mapper.readValue(responseBody.getFile(), new TypeReference<List<PlantInventoryEntryDTO>>() {
                });

        BusinessPeriodDTO rentalPeriod = BusinessPeriodDTO.of(LocalDate.now(), LocalDate.now().plusDays(2));

        PlantInventoryEntryDTO firstEntry = plantInventoryEntryDTOs.get(0);
        PlantInventoryEntry plant = PlantInventoryEntry.of(firstEntry.get_id(), firstEntry.getName(), firstEntry.getLinks().get(0).getHref());

        // Mock value when Rentit is called
        ResponseEntity<PurchaseOrderDTO> response = new ResponseEntity<PurchaseOrderDTO>(
                PurchaseOrderDTO.of(plant, rentalPeriod),
                HttpStatus.CREATED);
        response.getBody().add(new Link("hello"));
        response.getBody().set_id("1");
        response.getBody().setStatus(POStatus.OPEN);
        response.getBody().setTotal(new BigDecimal(30));
        return response;
    }
}
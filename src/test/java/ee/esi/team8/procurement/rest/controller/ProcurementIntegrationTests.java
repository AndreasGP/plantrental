package ee.esi.team8.procurement.rest.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import ee.esi.team8.ProcurementApplication;
import ee.esi.team8.common.application.dto.BusinessPeriodDTO;
import ee.esi.team8.hr.domain.model.EmployeeID;
import ee.esi.team8.procurement.application.dto.CommentDTO;
import ee.esi.team8.procurement.application.dto.CreatePHRDTO;
import ee.esi.team8.procurement.application.dto.PlantHireRequestDTO;
import ee.esi.team8.procurement.application.service.CommentAssembler;
import ee.esi.team8.procurement.application.service.ProcurementService;
import ee.esi.team8.procurement.domain.model.Comment;
import ee.esi.team8.procurement.domain.model.PHRStatus;
import ee.esi.team8.procurement.domain.model.PlantHireRequest;
import ee.esi.team8.procurement.repo.PlantHireRequestRepository;
import ee.esi.team8.rental.application.dto.PlantInventoryEntryDTO;
import ee.esi.team8.rental.application.dto.PurchaseOrderDTO;
import ee.esi.team8.rental.application.services.RentalService;
import ee.esi.team8.rental.domain.model.POStatus;
import ee.esi.team8.rental.domain.model.PlantInventoryEntry;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.transaction.Transactional;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {ProcurementApplication.class,
        ProcurementRestControllerTests.RentalServiceMock.class})
@WebAppConfiguration
@Transactional
public class ProcurementIntegrationTests {
    @Autowired
    private WebApplicationContext wac;
    private MockMvc mockMvc;
    private List<PlantInventoryEntryDTO> list;
    private BusinessPeriodDTO rentalPeriod;
    private EmployeeID siteEngineer;
    private EmployeeID worksEngineer;

    @Autowired
    @Qualifier("_halObjectMapper")
    ObjectMapper mapper;

    @Autowired
    RentalService rentalService;

    @Autowired
    ProcurementService procurementService;

    @Autowired
    CommentAssembler commentAssembler;

    @Autowired
    PlantHireRequestRepository plantHireRequestRepo;

    @Configuration
    static class RentalServiceMock {
        @Bean
        public RentalService rentalService() {
            return Mockito.mock(RentalService.class);
        }
    }

    @Before
    public void setup() throws IOException {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
        Resource responseBody = new ClassPathResource("trucks.json", this.getClass());
        list = mapper.readValue(responseBody.getFile(), new TypeReference<List<PlantInventoryEntryDTO>>() {});
        rentalPeriod = BusinessPeriodDTO.of(LocalDate.now(), LocalDate.now().plusDays(2));
        when(rentalService.findAvailablePlants("Truck", rentalPeriod.getStartDate(), rentalPeriod.getEndDate())).thenReturn(list);
        siteEngineer = EmployeeID.of("Reference to site engineer");
        worksEngineer = EmployeeID.of("Reference to works engineer");
    }

    @After
    public void tearOff() {
        plantHireRequestRepo.deleteAll();
    }

    @Test
    public void testPlantHireRequestAcceptedByRentit() throws Exception {
        //Mock setup
        PlantInventoryEntryDTO entry = list.get(0);
        PlantInventoryEntry plant = PlantInventoryEntry.of(entry.get_id(), entry.getName(), entry.getLinks().get(0).getHref());

        ResponseEntity<PurchaseOrderDTO> response = new ResponseEntity<PurchaseOrderDTO>(
                PurchaseOrderDTO.of(plant, rentalPeriod),
                HttpStatus.CREATED);
        response.getBody().add(new Link("hello"));
        response.getBody().set_id("1");
        response.getBody().setStatus(POStatus.OPEN);
        response.getBody().setTotal(new BigDecimal(30));

        when(rentalService.createPurchaseOrderAndReturnBody(any())).thenReturn(response.getBody());

        //(1) querying RentIt's plant catalog,
        MvcResult result = mockMvc.perform(
                get("/api/plants?name=Truck&startDate={start}&endDate={end}", rentalPeriod.getStartDate(), rentalPeriod.getEndDate()))
                .andExpect(status().isOk())
                .andReturn();
        List<PlantInventoryEntryDTO> availablePlants = mapper.readValue(
                result.getResponse().getContentAsString(),
                new TypeReference<List<PlantInventoryEntryDTO>>() {
                });

        //(2) selecting one plant for creating a Plant hire request,
        PlantInventoryEntryDTO firstEntry = availablePlants.get(0);
        PlantInventoryEntry firstPlant = PlantInventoryEntry.of(firstEntry.get_id(), firstEntry.getName(), firstEntry.getLinks().get(0).getHref());

        String constructionSiteId = "1";

        CreatePHRDTO phrDTO = CreatePHRDTO.of(rentalPeriod, firstEntry, constructionSiteId);

        result = mockMvc.perform(
                post("/api/requests")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(phrDTO)))
                .andExpect(status().isCreated())
                .andReturn();

        PlantHireRequestDTO createdPhr = mapper.readValue(result.getResponse().getContentAsString(), PlantHireRequestDTO.class);

        //(3) accepting the plant hire request (this should entail the creation of the Purchase order in RentIt's side), and
        mockMvc.perform(
                post("/api/requests/" + createdPhr.get_id() + "/accept")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString("")))
                .andExpect(status().isOk())
                .andReturn();

        //(4) checking the state of the Plant hire request after receiving the acceptance from RentIt's.
        PlantHireRequest phr = plantHireRequestRepo.getOne(createdPhr.get_id());

        assertEquals(PHRStatus.HIRED, phr.getStatus());
    }

    @Test
    public void testPlantHireRequestRejectedByRentit() throws Exception {
        //Mock setup
        PlantInventoryEntryDTO entry = list.get(0);
        PlantInventoryEntry plant = PlantInventoryEntry.of(entry.get_id(), entry.getName(), entry.getLinks().get(0).getHref());

        ResponseEntity<PurchaseOrderDTO> response = new ResponseEntity<PurchaseOrderDTO>(
                PurchaseOrderDTO.of(plant, rentalPeriod),
                HttpStatus.CREATED);
        response.getBody().add(new Link("hello"));
        response.getBody().set_id("1");
        response.getBody().setStatus(POStatus.REJECTED);
        response.getBody().setTotal(new BigDecimal(30));

        when(rentalService.createPurchaseOrderAndReturnBody(any())).thenReturn(response.getBody());

        //(1) querying RentIt's plant catalog,
        MvcResult result = mockMvc.perform(
                get("/api/plants?name=Truck&startDate={start}&endDate={end}", rentalPeriod.getStartDate(), rentalPeriod.getEndDate()))
                .andExpect(status().isOk())
                .andReturn();
        List<PlantInventoryEntryDTO> availablePlants = mapper.readValue(
                result.getResponse().getContentAsString(),
                new TypeReference<List<PlantInventoryEntryDTO>>() {
                });

        //(2) selecting one plant for creating a Plant hire request,
        PlantInventoryEntryDTO firstEntry = availablePlants.get(0);
        PlantInventoryEntry firstPlant = PlantInventoryEntry.of(firstEntry.get_id(), firstEntry.getName(), firstEntry.getLinks().get(0).getHref());
        String constructionSiteId = "1";
        CreatePHRDTO phrDTO = CreatePHRDTO.of(rentalPeriod, firstEntry, constructionSiteId);

        result = mockMvc.perform(
                post("/api/requests")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(phrDTO)))
                .andExpect(status().isCreated())
                .andReturn();

        PlantHireRequestDTO createdPhr = mapper.readValue(result.getResponse().getContentAsString(), PlantHireRequestDTO.class);

        //(3) accepting the plant hire request (this should entail the creation of the Purchase order in RentIt's side), and
        mockMvc.perform(
                post("/api/requests/" + createdPhr.get_id() + "/accept")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString("")))
                .andExpect(status().isOk())
                .andReturn();

        //(4) checking the state of the Plant hire request after receiving the rejection from RentIt's.
        PlantHireRequest phr = plantHireRequestRepo.getOne(createdPhr.get_id());

        assertEquals(PHRStatus.REJECTED, phr.getStatus());
    }

    @Test
    public void testPlantHireRequestRejectedByWorksEngineer() throws Exception {
        //(1) querying RentIt's plant catalog,
        MvcResult result = mockMvc.perform(
                get("/api/plants?name=Truck&startDate={start}&endDate={end}", rentalPeriod.getStartDate(), rentalPeriod.getEndDate()))
                .andExpect(status().isOk())
                .andReturn();
        List<PlantInventoryEntryDTO> availablePlants = mapper.readValue(
                result.getResponse().getContentAsString(),
                new TypeReference<List<PlantInventoryEntryDTO>>() {
                });

        //(2) selecting one plant for creating a Plant hire request,
        PlantInventoryEntryDTO firstEntry = availablePlants.get(0);
        PlantInventoryEntry firstPlant = PlantInventoryEntry.of(firstEntry.get_id(), firstEntry.getName(), firstEntry.getLinks().get(0).getHref());
        String constructionSiteId = "1";

        CreatePHRDTO phrDTO = CreatePHRDTO.of(rentalPeriod, firstEntry, constructionSiteId);

        result = mockMvc.perform(
                post("/api/requests")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(phrDTO)))
                .andExpect(status().isCreated())
                .andReturn();

        PlantHireRequestDTO createdPhr = mapper.readValue(result.getResponse().getContentAsString(), PlantHireRequestDTO.class);

        //(3) rejecting the plant hire request
        PlantHireRequest rejectingPhr = plantHireRequestRepo.findOne(createdPhr.get_id());
        CommentDTO commentDTO = commentAssembler.toResource(Comment.of(worksEngineer, "No comments", rejectingPhr));

        mockMvc.perform(
                post("/api/requests/" + createdPhr.get_id() + "/reject")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(commentDTO)))
                .andExpect(status().isOk())
                .andReturn();

        PlantHireRequest phr = plantHireRequestRepo.getOne(createdPhr.get_id());

        assertEquals(PHRStatus.REJECTED, phr.getStatus());
    }
}

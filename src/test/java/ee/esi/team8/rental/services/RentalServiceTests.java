package ee.esi.team8.rental.services;

import ee.esi.team8.ProcurementApplication;
import ee.esi.team8.procurement.rest.controller.ProcurementRestControllerTests;
import ee.esi.team8.rental.application.services.RentalService;
import ee.esi.team8.rental.domain.model.PurchaseOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import javax.transaction.Transactional;
import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@SpringBootTest(classes = {ProcurementApplication.class})
@Transactional
public class RentalServiceTests {

    @Autowired
    RentalService rentalService;

    @Test
    public void testPurchaseOrderHrefToIdParsing() throws Exception {
        //Test most common format of a href
        PurchaseOrder po1 = PurchaseOrder.of("http://localhost:8080/orders/74eeb615-2e85-4586-9673-8df91be1e1c9", new BigDecimal(0));
        String id1 = rentalService.getPurchaseOrderId(po1);
        assertEquals("74eeb615-2e85-4586-9673-8df91be1e1c9", id1);

        //Test trailing slash
        PurchaseOrder po2 = PurchaseOrder.of("http://localhost:8080/orders/74eeb615-2e85-4586-9673-8df91be1e1c9/", new BigDecimal(0));
        String id2 = rentalService.getPurchaseOrderId(po2);
        assertEquals("74eeb615-2e85-4586-9673-8df91be1e1c9", id2);

        //Test plain id href
        PurchaseOrder po3 = PurchaseOrder.of("74eeb615-2e85-4586-9673-8df91be1e1c9", new BigDecimal(0));
        String id3 = rentalService.getPurchaseOrderId(po3);
        assertEquals("74eeb615-2e85-4586-9673-8df91be1e1c9", id3);

        //Test plain numeric href
        PurchaseOrder po4 = PurchaseOrder.of("4", new BigDecimal(0));
        String id4 = rentalService.getPurchaseOrderId(po4);
        assertEquals("4", id4);

    }
}
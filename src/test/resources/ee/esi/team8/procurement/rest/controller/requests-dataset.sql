INSERT INTO plant_hire_request (id, construction_site_id, plantid, plant_name, plant_href, purchase_order_total, purchase_order_href, start_date, end_date, extension_end_date, status, supplier)
                        VALUES (1, '1', '1', 'Mini excavator', '', 250.00, '', '2017-03-18', '2017-03-20', NULL, 'CLOSED', 'RentIt');
INSERT INTO plant_hire_request (id, construction_site_id, plantid, plant_name, plant_href, purchase_order_total, purchase_order_href, start_date, end_date, extension_end_date, status, supplier)
                        VALUES (2, '1', '1', 'Mini excavator', '', 250.00, '', '2017-03-22', '2017-03-24', NULL, 'PENDING', 'RentIt');
INSERT INTO plant_hire_request (id, construction_site_id, plantid, plant_name, plant_href, purchase_order_total, purchase_order_href, start_date, end_date, extension_end_date, status, supplier)
                        VALUES (3, '2', '1', 'Mini excavator', '', 250.00, 'http://localhost:8083/api/orders/420', '2017-03-18', '2017-03-20', NULL, 'HIRED', 'RentIt');
INSERT INTO plant_hire_request (id, construction_site_id, plantid, plant_name, plant_href, purchase_order_total, purchase_order_href, start_date, end_date, extension_end_date, status, supplier)
                        VALUES (4, '2', '1', 'Mini excavator', '', 250.00, '', '2017-03-18', '2017-03-20', NULL, 'REJECTED', 'RentIt');
INSERT INTO plant_hire_request (id, construction_site_id, plantid, plant_name, plant_href, purchase_order_total, purchase_order_href, start_date, end_date, extension_end_date, status, supplier)
                        VALUES (5, '3', '1', 'Mini excavator', '', 250.00, '', '2017-03-18', '2017-03-20', NULL, 'ACCEPTED', 'RentIt');
INSERT INTO plant_hire_request (id, construction_site_id, plantid, plant_name, plant_href, purchase_order_total, purchase_order_href, start_date, end_date, extension_end_date, status, supplier)
                        VALUES (6, '3', '1', 'Mini excavator', '', 250.00, '', '2017-03-22', '2017-03-24', NULL, 'PENDING', 'RentIt');
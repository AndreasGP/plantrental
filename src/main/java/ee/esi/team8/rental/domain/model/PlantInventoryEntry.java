package ee.esi.team8.rental.domain.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Value;

import javax.persistence.Embeddable;

/**
 * Hold the reference to resources in RentIt.
 * However, there is some additional information to avoid an interaction with RentIt’s
 * information system when we need to render something in BuildIt’s side.
 * (First appearance: hw4)
 */
@Embeddable
@Value
@NoArgsConstructor(force = true, access = AccessLevel.PRIVATE)
@AllArgsConstructor(staticName = "of")
public class PlantInventoryEntry {
    String id;
    String name;
    String href;
}

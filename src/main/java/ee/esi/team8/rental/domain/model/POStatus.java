package ee.esi.team8.rental.domain.model;

public enum POStatus {
    PENDING("Pending"), REJECTED("Rejected"), OPEN("Open"), CLOSED("Closed");

    private String statusString;

    POStatus(String statusString) {
        this.statusString = statusString;
    }

    @Override
    public String toString() {
        return statusString;
    }
}

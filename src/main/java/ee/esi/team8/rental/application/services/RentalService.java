package ee.esi.team8.rental.application.services;

import ee.esi.team8.procurement.application.dto.PlantHireRequestDTO;
import ee.esi.team8.rental.application.dto.PartialPurchaseOrderDTO;
import ee.esi.team8.rental.application.dto.PlantInventoryEntryDTO;
import ee.esi.team8.rental.application.dto.PurchaseOrderDTO;
import ee.esi.team8.rental.application.dto.PurchaseOrderExtensionDTO;
import ee.esi.team8.rental.domain.model.PurchaseOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

@Service
public class RentalService {
    @Autowired
    RestTemplate restTemplate;

    // Comment out that rentitUrl that is not needed
    private String rentitUrl = "http://private-6c8b5-team8esirentit.apiary-mock.com";
    // private String rentitUrl = "http://localhost:8090";

    public List<PlantInventoryEntryDTO> findAvailablePlants(String plantName, LocalDate startDate, LocalDate endDate) {
        PlantInventoryEntryDTO[] plants = restTemplate.getForObject(
                rentitUrl + "/api/inventory/plants?name={name}&startDate={start}&endDate={end}",
                PlantInventoryEntryDTO[].class, plantName, startDate, endDate);
        return Arrays.asList(plants);
    }

    public List<PlantInventoryEntryDTO> findAllAvailablePlants() {
        PlantInventoryEntryDTO[] plants = restTemplate.getForObject(
                rentitUrl + "/api/inventory/plants",
                PlantInventoryEntryDTO[].class);
        return Arrays.asList(plants);
    }

    public List<PurchaseOrderDTO> findSubmittedPurchaseOrders() {
        PurchaseOrderDTO[] purchaseOrders = restTemplate.getForObject(
                rentitUrl + "/api/sales/orders",
                PurchaseOrderDTO[].class);
        return Arrays.asList(purchaseOrders);
    }

    public PurchaseOrderDTO findPurchaseOrder(String id) {
        PurchaseOrderDTO purchaseOrder = restTemplate.getForObject(
                rentitUrl + "/api/sales/orders/{id}",
                PurchaseOrderDTO.class, id);
        return purchaseOrder;
    }

    public ResponseEntity<PurchaseOrderDTO> createPurchaseOrder(PlantHireRequestDTO hireRequestDTO) {
        PartialPurchaseOrderDTO orderDTO = PartialPurchaseOrderDTO.of(hireRequestDTO.getPlant(), hireRequestDTO.getHirePeriod());
        return restTemplate.postForEntity(rentitUrl + "/api/sales/orders", orderDTO, PurchaseOrderDTO.class);
    }

    /**
     * Sends the PO extension intent to RentIt. Response is expected to be an updated PurchaseOrder. Appeared in HW6.
     *
     * @param poExtensionDTO
     * @return
     */
    public PurchaseOrderDTO extendPurchaseOrder(String id, PurchaseOrderExtensionDTO poExtensionDTO) {
        return restTemplate.postForObject(rentitUrl + "/api/sales/orders/{id}/extensions", poExtensionDTO, PurchaseOrderDTO.class, id);
    }

    /*
    I am ashamed for this method name Tõnis Kasekamp
     */
    public PurchaseOrderDTO createPurchaseOrderAndReturnBody(PlantHireRequestDTO hireRequestDTO) {
        ResponseEntity<PurchaseOrderDTO> response = createPurchaseOrder(hireRequestDTO);
        return response.getBody();
    }

    public ResponseEntity<PurchaseOrderDTO> resubmitPurchaseOrder(PurchaseOrderDTO purchaseOrderDTO) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<PurchaseOrderDTO> entity = new HttpEntity<>(purchaseOrderDTO, headers);
        return restTemplate.exchange(rentitUrl + "/api/sales/orders", HttpMethod.PUT, entity, PurchaseOrderDTO.class);
    }

    public ResponseEntity<PurchaseOrderDTO> cancelPurchaseOrder(String id) {
        return restTemplate.exchange(rentitUrl + "/api/sales/orders/{id}", HttpMethod.DELETE, null, PurchaseOrderDTO.class, id);
    }

    /**
     * Extracts the id from PurchaseOrder's href. First appearance in HW6.
     *
     * @param po PurchaseOrder of which id to get
     * @return id of the purchase order
     */
    public String getPurchaseOrderId(PurchaseOrder po) {
        String href = po.getHref();
        //Strip trailing / if there is one
        if (href.endsWith("/")) {
            href = href.substring(0, href.length() - 1);
        }
        //Find the last position of /
        int lastSlash = href.lastIndexOf('/');
        return href.substring(lastSlash + 1, href.length());
    }
}

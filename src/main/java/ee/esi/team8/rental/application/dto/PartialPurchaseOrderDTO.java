package ee.esi.team8.rental.application.dto;

import ee.esi.team8.common.application.dto.BusinessPeriodDTO;
import ee.esi.team8.rental.domain.model.POStatus;
import ee.esi.team8.rental.domain.model.PlantInventoryEntry;
import ee.esi.team8.rental.domain.model.PurchaseOrder;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.hateoas.ResourceSupport;

import java.math.BigDecimal;

@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@Data
public class PartialPurchaseOrderDTO extends ResourceSupport {
    PlantInventoryEntry plant;
    BusinessPeriodDTO rentalPeriod;

    public static PartialPurchaseOrderDTO of(PlantInventoryEntry plant, BusinessPeriodDTO rentalPeriod) {
        PartialPurchaseOrderDTO po = new PartialPurchaseOrderDTO();
        po.plant = plant;
        po.rentalPeriod = rentalPeriod;
        return po;
    }
}


package ee.esi.team8.rental.application.dto;

import ee.esi.team8.common.application.dto.BusinessPeriodDTO;
import ee.esi.team8.rental.domain.model.POStatus;
import ee.esi.team8.rental.domain.model.PlantInventoryEntry;
import ee.esi.team8.rental.domain.model.PurchaseOrder;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.hateoas.ResourceSupport;

import java.math.BigDecimal;

@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@Data
public class PurchaseOrderDTO extends ResourceSupport {
    String _id;
    PlantInventoryEntry plant;
    BusinessPeriodDTO rentalPeriod;
    BigDecimal total;
    POStatus status;

    public static PurchaseOrderDTO of(PlantInventoryEntry plant, BusinessPeriodDTO rentalPeriod) {
        PurchaseOrderDTO po = new PurchaseOrderDTO();
        po.plant = plant;
        po.rentalPeriod = rentalPeriod;

        return po;
    }
}

package ee.esi.team8.rental.application.dto;

import ee.esi.team8.common.application.dto.BusinessPeriodDTO;
import ee.esi.team8.rental.domain.model.POStatus;
import ee.esi.team8.rental.domain.model.PlantInventoryEntry;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.hateoas.ResourceSupport;

import java.math.BigDecimal;
import java.time.LocalDate;

@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
@Data
public class PurchaseOrderExtensionDTO extends ResourceSupport {
    LocalDate extensionEndDate;
}

package ee.esi.team8.procurement.domain.model;

/**
 * Status of Plant Hire Request
 * (First appearance: hw4)
 */
public enum PHRStatus {
    PENDING("Pending"), ACCEPTED("Accepted"), HIRED("Hired"), CLOSED("Closed"), REJECTED("Rejected"), CANCELLED("Cancelled");

    private String statusString;

    PHRStatus(String statusString) {
        this.statusString = statusString;
    }

    @Override
    public String toString() {
        return statusString;
    }
}

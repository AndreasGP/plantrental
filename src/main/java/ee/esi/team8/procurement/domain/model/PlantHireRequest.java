package ee.esi.team8.procurement.domain.model;

import ee.esi.team8.common.domain.model.BusinessPeriod;
import ee.esi.team8.hr.domain.model.EmployeeID;
import ee.esi.team8.procurement.domain.infrastructure.ProcurementIdentifierFactory;
import ee.esi.team8.rental.domain.model.PlantInventoryEntry;
import ee.esi.team8.rental.domain.model.PurchaseOrder;
import ee.esi.team8.rental.domain.model.Supplier;
import lombok.Data;
import lombok.Getter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * When site engineer requires an available plant, Plant Hire Request (PHR) is created.
 * PHR includes
 * the identifier of the construction site where the plant is needed,
 * the supplier of the plant,
 * the plant’s identifier,
 * the expected start and end date of the hire period and
 * the cost of hiring the plant for this period of time.
 * the name of the site engineer making the request.
 * comments (If the works engineer rejects or changes the PHR)
 * (First appearance: hw4)
 * (Modified in hw4: milestone 2)
 */
@Entity
@Data
@Getter
public class PlantHireRequest {
    @Id
    String id;

    String constructionSiteId;

    @OneToMany(cascade = {CascadeType.ALL},mappedBy = "plantHireRequest")
    List<Comment> comments;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name="href", column=@Column(name="supplier"))})
    Supplier supplier;


    @Embedded
    @AttributeOverrides({@AttributeOverride(name="href", column=@Column(name="siteEngineer"))})
    EmployeeID siteEngineer;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name="href", column=@Column(name="worksEngineer"))})
    EmployeeID worksEngineer;

    /**
     * The expected start and end date of the hire period
     */
    @Embedded
    BusinessPeriod rentalPeriod;

    /**
     * The end of the requested extension period. Only not null if an extension has been requested but it has received no reply.
     * First appearance in HW6.
     */
    LocalDate extensionEndDate;

    /**
     * The "cost of hiring the plant for this period of time" stored to reduce round trips.
     */
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "href", column = @Column(name = "purchaseOrderHref")),
            @AttributeOverride(name = "total", column = @Column(name = "purchaseOrderTotal"))})
    PurchaseOrder purchaseOrder;

    /**
     * The "plant’s identifier" stored to reduce round trips.
     */
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "href", column = @Column(name = "plantHref")),
            @AttributeOverride(name = "name", column = @Column(name = "plantName")),
            @AttributeOverride(name = "id", column = @Column(name = "plantID"))})
    PlantInventoryEntry plant;

    @Enumerated(EnumType.STRING)
    PHRStatus status;

    public static PlantHireRequest of(PlantInventoryEntry plant, BusinessPeriod rentalPeriod, EmployeeID siteEngineer, String constructionSiteId) {
        PlantHireRequest phRequest = new PlantHireRequest();
        phRequest.id = ProcurementIdentifierFactory.nextPlantHireRequestID();
        phRequest.plant = plant;
        phRequest.siteEngineer = siteEngineer;
        phRequest.rentalPeriod = rentalPeriod;
        phRequest.status = PHRStatus.PENDING;
        phRequest.constructionSiteId = constructionSiteId;
        phRequest.comments = new ArrayList<>();

        return phRequest;
    }

    public void acceptPHR() {
        this.status = PHRStatus.ACCEPTED;
    }

    public void acceptPHRbyEngineer(EmployeeID worksEngineer) {
        this.status = PHRStatus.ACCEPTED;
        this.worksEngineer = worksEngineer;
    }

    public void hirePHR() {
        this.status = PHRStatus.HIRED;
    }

    public void rejectPHR() {
        this.status = PHRStatus.REJECTED;
    }

    public void cancelPHR() {
        this.status = PHRStatus.CANCELLED;
    }

    public void setWorksEngineer(EmployeeID worksEngineer) {
        this.worksEngineer = worksEngineer;
    }

    public void updatePurcaseOrder(PurchaseOrder order) {
        this.purchaseOrder = order;
    }
}

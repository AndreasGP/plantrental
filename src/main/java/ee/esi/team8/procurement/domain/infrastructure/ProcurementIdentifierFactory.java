package ee.esi.team8.procurement.domain.infrastructure;
import org.springframework.stereotype.Service;

import java.util.UUID;

/**
 * Identifier generators for entities in Procurement bounded context
 * (First appearance: hw4)
 */
@Service
public class ProcurementIdentifierFactory {
    /**
     * @return Identifier for Plant Hire Request
     */
    public static String nextPlantHireRequestID() {
        return UUID.randomUUID().toString();
    }

    /**
     * @return Identifier for Comment
     */
    public static String nextCommentID() {
        return UUID.randomUUID().toString();
    }
}

package ee.esi.team8.procurement.domain.model;

import ee.esi.team8.hr.domain.model.EmployeeID;
import ee.esi.team8.procurement.domain.infrastructure.ProcurementIdentifierFactory;
import lombok.Data;
import lombok.Getter;


import javax.persistence.*;

/**
 * (First appearance: hw4: milestone 2)
 */
@Entity
@Getter
@Data
public class Comment {
    @Id
    String id;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name="href", column=@Column(name="engineer"))})
    EmployeeID engineer;

    String text;

    @ManyToOne
    PlantHireRequest plantHireRequest;

    public static Comment of(EmployeeID worksEngineer, String text, PlantHireRequest plantHireRequest) {
        Comment comment = new Comment();
        comment.id = ProcurementIdentifierFactory.nextCommentID();
        comment.text = text;
        comment.engineer = worksEngineer;
        comment.plantHireRequest = plantHireRequest;
        return comment;
    }
}


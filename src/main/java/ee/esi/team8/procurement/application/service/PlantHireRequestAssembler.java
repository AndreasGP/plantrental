package ee.esi.team8.procurement.application.service;

import ee.esi.team8.common.application.dto.BusinessPeriodDTO;
import ee.esi.team8.procurement.application.dto.PlantHireRequestDTO;
import ee.esi.team8.procurement.domain.model.Comment;
import ee.esi.team8.procurement.domain.model.PlantHireRequest;
import ee.esi.team8.procurement.rest.controller.ProcurementRestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
public class PlantHireRequestAssembler extends ResourceAssemblerSupport<PlantHireRequest, PlantHireRequestDTO> {
    @Autowired
    CommentAssembler commentAssembler;

    public PlantHireRequestAssembler() {
        super(ProcurementRestController.class, PlantHireRequestDTO.class);
    }
    public PlantHireRequestDTO toResource(PlantHireRequest plantHireRequest) {
        PlantHireRequestDTO dto = createResourceWithId(plantHireRequest.getId(), plantHireRequest);
        dto.set_id(plantHireRequest.getId());
        dto.setConstructionSiteId(plantHireRequest.getConstructionSiteId());
        dto.setHirePeriod(BusinessPeriodDTO.of(plantHireRequest.getRentalPeriod().getStartDate(),plantHireRequest.getRentalPeriod().getEndDate()));
        dto.setOrder(plantHireRequest.getPurchaseOrder());
        dto.setPlant(plantHireRequest.getPlant());
        dto.setStatus(plantHireRequest.getStatus());
        dto.setSiteEngineer(plantHireRequest.getSiteEngineer());
        if(plantHireRequest.getComments() != null) {
            dto.setComments(plantHireRequest.getComments().stream().map(c -> commentAssembler.toResource(c)).collect(Collectors.toList()));
        }
        if(plantHireRequest.getExtensionEndDate() != null) {
            dto.setExtensionEndDate(plantHireRequest.getExtensionEndDate());
        }
        return dto;
    }
}

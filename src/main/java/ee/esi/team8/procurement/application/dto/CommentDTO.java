package ee.esi.team8.procurement.application.dto;

import ee.esi.team8.common.rest.ResourceSupport;
import ee.esi.team8.hr.domain.model.EmployeeID;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * (First appearance: hw4: milestone 2)
 */
@EqualsAndHashCode(callSuper = false)
@Data
public class CommentDTO extends ResourceSupport {
    String text;
    EmployeeID engineer;
}

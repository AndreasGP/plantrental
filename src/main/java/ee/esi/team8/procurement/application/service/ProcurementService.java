package ee.esi.team8.procurement.application.service;

import ee.esi.team8.common.application.dto.BusinessPeriodDTO;
import ee.esi.team8.common.domain.model.BusinessPeriod;
import ee.esi.team8.hr.domain.model.EmployeeID;
import ee.esi.team8.procurement.application.dto.CommentDTO;
import ee.esi.team8.procurement.application.dto.CreatePHRDTO;
import ee.esi.team8.procurement.application.dto.ExtendPHRDTO;
import ee.esi.team8.procurement.application.dto.PlantHireRequestDTO;
import ee.esi.team8.procurement.domain.model.Comment;
import ee.esi.team8.procurement.domain.model.PHRStatus;
import ee.esi.team8.procurement.domain.model.PlantHireRequest;
import ee.esi.team8.procurement.repo.CommentRepository;
import ee.esi.team8.procurement.repo.PlantHireRequestRepository;
import ee.esi.team8.rental.application.dto.PurchaseOrderDTO;
import ee.esi.team8.rental.application.dto.PurchaseOrderExtensionDTO;
import ee.esi.team8.rental.application.services.RentalService;
import ee.esi.team8.rental.domain.model.POStatus;
import ee.esi.team8.rental.domain.model.PlantInventoryEntry;
import ee.esi.team8.rental.domain.model.PurchaseOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

/**
 * Service to interact with the procurement bounded context
 * (First appearance: hw4)
 */
@Service
public class ProcurementService {
    @Autowired
    PlantHireRequestRepository plantHireRequestRepository;

    @Autowired
    CommentRepository commentRepository;

    @Autowired
    PlantHireRequestAssembler plantHireRequestAssembler;

    @Autowired
    RentalService rentalService;

    public List<PlantHireRequestDTO> findAll() {
        return plantHireRequestAssembler.toResources(plantHireRequestRepository.findAll());
    }

    public List<PlantHireRequestDTO> findWithStatus(PHRStatus status) {
        return plantHireRequestAssembler.toResources(plantHireRequestRepository.findAllWithStatus(status));
    }

    public PlantHireRequestDTO findOne(String id) {
        return plantHireRequestAssembler.toResource(plantHireRequestRepository.findOne(id));
    }

    public PlantHireRequestDTO createPlantHireRequest(CreatePHRDTO phrDTO, EmployeeID siteEngineer) {
        BusinessPeriod businessPeriod = BusinessPeriod.of(phrDTO.getHirePeriod().getStartDate(), phrDTO.getHirePeriod().getEndDate());
        PlantInventoryEntry plant = PlantInventoryEntry.of(phrDTO.getPlant().get_id(), phrDTO.getPlant().getName(), phrDTO.getPlant().getLink("self").getHref());
        PlantHireRequest phr = PlantHireRequest.of(plant, businessPeriod, siteEngineer, phrDTO.getConstructionSiteId());
        plantHireRequestRepository.save(phr);

        return plantHireRequestAssembler.toResource(phr);
    }

    public PlantHireRequestDTO updatePlantHireRequest(String phrID, CreatePHRDTO phrDTO, EmployeeID siteEngineer) {
        PlantHireRequest phr = plantHireRequestRepository.findOne(phrID);
        if(phr.getStatus() == PHRStatus.PENDING || phr.getStatus() == PHRStatus.REJECTED) {
            BusinessPeriod rentalPeriod = BusinessPeriod.of(phrDTO.getHirePeriod().getStartDate(), phrDTO.getHirePeriod().getEndDate());
            phr.setRentalPeriod(rentalPeriod);
            PlantInventoryEntry plant = PlantInventoryEntry.of(phrDTO.getPlant().get_id(), phrDTO.getPlant().getName(), phrDTO.getPlant().getLink("self").getHref());
            phr.setPlant(plant);
            phr.setConstructionSiteId(phrDTO.getConstructionSiteId());
            phr.setSiteEngineer(siteEngineer);
            phr.setStatus(PHRStatus.PENDING);
            plantHireRequestRepository.save(phr);
        }
        return plantHireRequestAssembler.toResource(phr);
    }

    public void extendPlantHireRequest(String phrID, LocalDate extensionEndDate, EmployeeID siteEngineer) {
        PlantHireRequest phr = plantHireRequestRepository.findOne(phrID);
        if(phr.getStatus() == PHRStatus.HIRED && extensionEndDate.isAfter(phr.getRentalPeriod().getEndDate())) {
            phr.setExtensionEndDate(extensionEndDate);
            plantHireRequestRepository.save(phr);
            String purchaseOrderId = rentalService.getPurchaseOrderId(phr.getPurchaseOrder());
            PurchaseOrderExtensionDTO poExtensionDTO = PurchaseOrderExtensionDTO.of(extensionEndDate);
            rentalService.extendPurchaseOrder(purchaseOrderId, poExtensionDTO);
        }
    }

    /**
     * Accepts a PlantHireRequest extension coming from an external system. The external system notifies the acceptance with the PHR's PO id. Added in HW6.
     * @param purchaseOrderId The Purchase Order Id associated with the PHR that was accepted.
     */
    public void acceptPlantHireRequestExtension(String purchaseOrderId) {
        PlantHireRequest phr = plantHireRequestRepository.findByPurchaseOrderId(purchaseOrderId);
        if(phr.getExtensionEndDate() != null) {
            phr.setRentalPeriod(BusinessPeriod.of(phr.getRentalPeriod().getStartDate(), phr.getExtensionEndDate()));
            phr.setExtensionEndDate(null);
            plantHireRequestRepository.save(phr);
        }
    }

    /**
     * Rejects a PlantHireRequest extension coming from an external system. The external system notifies the rejection with the PHR's PO id. Added in HW6.
     * @param purchaseOrderId The Purchase Order Id associated with the PHR that was rejected.
     */
    public void rejectPlantHireRequestExtension(String purchaseOrderId) {
        PlantHireRequest phr = plantHireRequestRepository.findByPurchaseOrderId(purchaseOrderId);
        if(phr.getExtensionEndDate() != null) {
            phr.setExtensionEndDate(null);
            plantHireRequestRepository.save(phr);
        }
    }

    /**
     * Accepts @{@link PlantHireRequest}. Contacts Rentit with a {@link PurchaseOrder}. Changes status based on PO status.
     *
     * @param phrID         @{@link PlantHireRequest#getId()}
     * @param worksEngineer @{@link EmployeeID}
     */
    public PlantHireRequest acceptPlantHireRequest(String phrID, EmployeeID worksEngineer) {
        PlantHireRequest phr = plantHireRequestRepository.findOne(phrID);
        if(phr.getStatus() == PHRStatus.PENDING) {
            phr.acceptPHRbyEngineer(worksEngineer);
            plantHireRequestRepository.save(phr);
            PurchaseOrderDTO dto = rentalService.createPurchaseOrderAndReturnBody(plantHireRequestAssembler.toResource(phr));
            updatePHRwithPurchaseOrder(phr, dto);
        }
        return phr;
    }

    private void updatePHRwithPurchaseOrder(PlantHireRequest phr, PurchaseOrderDTO dto) {
        PurchaseOrder po = PurchaseOrder.of(dto.getLinks().get(0).getHref(), dto.getTotal());
        phr.updatePurcaseOrder(po);


        switch (dto.getStatus()) {
            case OPEN:
                phr.hirePHR();
                break;
            case REJECTED:
                phr.rejectPHR();
                break;
            default:
                break;
        }

        plantHireRequestRepository.save(phr);
    }

    public void rejectPlantHireRequest(String phrID, CommentDTO commentDTO, EmployeeID worksEngineer) {
        PlantHireRequest hireRequest = plantHireRequestRepository.findOne(phrID);
        if(hireRequest.getStatus() == PHRStatus.PENDING) {
            hireRequest.rejectPHR();
            hireRequest.setWorksEngineer(worksEngineer);
            Comment comment = Comment.of(worksEngineer, commentDTO.getText(), hireRequest);
            List<Comment> previousComments = hireRequest.getComments();
            previousComments.add(comment);
            hireRequest.setComments(previousComments);
            plantHireRequestRepository.save(hireRequest);
            commentRepository.save(comment);
        }
    }

    public void cancelPlantHireRequest(String phrID, EmployeeID worksEngineer) {
        PlantHireRequest hireRequest = plantHireRequestRepository.findOne(phrID);
        if(hireRequest.getStatus() == PHRStatus.ACCEPTED || hireRequest.getStatus() == PHRStatus.HIRED) {
            hireRequest.cancelPHR();
            hireRequest.setWorksEngineer(worksEngineer);
            plantHireRequestRepository.save(hireRequest);
        }
    }
}

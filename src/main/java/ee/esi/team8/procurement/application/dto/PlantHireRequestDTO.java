package ee.esi.team8.procurement.application.dto;

import ee.esi.team8.common.application.dto.BusinessPeriodDTO;
import ee.esi.team8.common.rest.ResourceSupport;
import ee.esi.team8.hr.domain.model.EmployeeID;
import ee.esi.team8.procurement.domain.model.PHRStatus;
import ee.esi.team8.rental.domain.model.PlantInventoryEntry;
import ee.esi.team8.rental.domain.model.PurchaseOrder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDate;
import java.util.List;

@EqualsAndHashCode(callSuper = false)
@Data
public class PlantHireRequestDTO extends ResourceSupport {
    String _id;
    String constructionSiteId;
    BusinessPeriodDTO hirePeriod;
    PurchaseOrder order;
    PlantInventoryEntry plant;
    PHRStatus status;
    EmployeeID siteEngineer;
    List<CommentDTO> comments;
    LocalDate extensionEndDate;
}

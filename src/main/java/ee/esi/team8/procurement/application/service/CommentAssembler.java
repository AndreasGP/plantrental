package ee.esi.team8.procurement.application.service;

import ee.esi.team8.procurement.application.dto.CommentDTO;
import ee.esi.team8.procurement.domain.model.Comment;
import ee.esi.team8.procurement.rest.controller.ProcurementRestController;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Service;

@Service
public class CommentAssembler extends ResourceAssemblerSupport<Comment, CommentDTO> {
    public CommentAssembler() {
        super(ProcurementRestController.class, CommentDTO.class);
    }
    public CommentDTO toResource(Comment comment) {
        CommentDTO dto = createResourceWithId(comment.getId(), comment);
        dto.setEngineer(comment.getEngineer());
        dto.setText(comment.getText());
        return dto;
    }
}
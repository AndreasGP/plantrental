package ee.esi.team8.procurement.application.dto;

import ee.esi.team8.common.application.dto.BusinessPeriodDTO;
import ee.esi.team8.common.rest.ResourceSupport;
import ee.esi.team8.hr.domain.model.EmployeeID;
import ee.esi.team8.rental.domain.model.PlantInventoryEntry;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor(staticName = "of")
@NoArgsConstructor(force = true)
@Data
public class ExtendPHRDTO extends ResourceSupport {
    LocalDate extensionEndDate;
}

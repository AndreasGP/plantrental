package ee.esi.team8.procurement.rest.controller;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import ee.esi.team8.hr.domain.model.EmployeeID;
import ee.esi.team8.procurement.application.dto.CommentDTO;
import ee.esi.team8.procurement.application.dto.CreatePHRDTO;
import ee.esi.team8.procurement.application.dto.ExtendPHRDTO;
import ee.esi.team8.procurement.application.dto.PlantHireRequestDTO;
import ee.esi.team8.procurement.application.service.ProcurementService;
import ee.esi.team8.procurement.domain.model.PHRStatus;
import ee.esi.team8.procurement.domain.model.PlantHireRequest;
import ee.esi.team8.rental.application.dto.PlantInventoryEntryDTO;
import ee.esi.team8.rental.application.dto.PurchaseOrderDTO;
import ee.esi.team8.rental.application.services.RentalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.Embedded;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class ProcurementRestController {
    @Autowired
    RentalService rentalService;

    @Autowired
    ProcurementService procurementService;

    @GetMapping("/requests")
    public List<PlantHireRequestDTO> getAllPlantHireRequests(@RequestParam(name = "status", required = false) Optional<String> status) throws Exception {
        if(status.isPresent()) {
            try {
                PHRStatus phrStatus = PHRStatus.valueOf(status.get());
                return procurementService.findWithStatus(phrStatus);
            } catch (IllegalArgumentException iae) {
                return null;
            }
        } else {
            return procurementService.findAll();
        }
    }

    @GetMapping("/requests/{id}")
    public PlantHireRequestDTO findPlantHireRequest(@PathVariable("id") String id) {
        return procurementService.findOne(id);
    }

    @PostMapping("/requests")
    public ResponseEntity<PlantHireRequestDTO> createPlantHireRequest(@RequestBody CreatePHRDTO phrDTO) throws Exception {
        EmployeeID siteEngineer = EmployeeID.of("siteEngineerRef");
        return new ResponseEntity<>(procurementService.createPlantHireRequest(phrDTO, siteEngineer), HttpStatus.CREATED);
    }

    @PatchMapping("/requests/{id}")
    public ResponseEntity<PlantHireRequestDTO> updatePlantHireRequest(@PathVariable("id") String phrId, @RequestBody CreatePHRDTO phrDTO) throws Exception {
        EmployeeID siteEngineer = EmployeeID.of("siteEngineerRef");
        return new ResponseEntity<>(procurementService.updatePlantHireRequest(phrId, phrDTO, siteEngineer), HttpStatus.CREATED);
    }

    @PostMapping("/requests/{id}/accept")
    public ResponseEntity<String> acceptPHR(@PathVariable("id") String id) throws Exception {
        EmployeeID worksEngineer = EmployeeID.of("worksEngineerRef");
        procurementService.acceptPlantHireRequest(id, worksEngineer);
        return new ResponseEntity(new EmptyJsonResponse(), HttpStatus.OK);
    }

    @PostMapping("/requests/{id}/reject")
    public ResponseEntity rejectPHR (@PathVariable("id") String id, @RequestBody CommentDTO commentDTO){
        EmployeeID worksEngineer = EmployeeID.of("worksEngineerRef");
        procurementService.rejectPlantHireRequest(id, commentDTO, worksEngineer);
        return new ResponseEntity(new EmptyJsonResponse(), HttpStatus.OK);
    }

    @PostMapping("/requests/{id}/cancel")
    public ResponseEntity cancelPHR (@PathVariable("id") String id){
        EmployeeID worksEngineer = EmployeeID.of("worksEngineerRef");
        procurementService.cancelPlantHireRequest(id, worksEngineer);
        return new ResponseEntity(new EmptyJsonResponse(), HttpStatus.OK);
    }

    @PatchMapping("requests/{id}/extend")
    public PlantHireRequestDTO extendPHR(@PathVariable("id") String id, @RequestBody ExtendPHRDTO extendDTO) {
        EmployeeID siteEngineer = EmployeeID.of("siteEngineerRef");
        procurementService.extendPlantHireRequest(id, extendDTO.getExtensionEndDate(), siteEngineer);
        return procurementService.findOne(id);
    }

    @PostMapping("/orders/{id}/acceptExtension")
    public ResponseEntity<String> acceptPOExtension(@PathVariable("id") String purchaseOrderId) throws Exception {
        procurementService.acceptPlantHireRequestExtension(purchaseOrderId);
        return new ResponseEntity(new EmptyJsonResponse(), HttpStatus.OK);
    }

    @DeleteMapping("/orders/{id}/rejectExtension")
    public ResponseEntity<String> rejectPOExtension(@PathVariable("id") String purchaseOrderId) throws Exception {
        procurementService.rejectPlantHireRequestExtension(purchaseOrderId);
        return new ResponseEntity(new EmptyJsonResponse(), HttpStatus.OK);
    }

    @GetMapping("/plants")
    public List<PlantInventoryEntryDTO> findAvailablePlants(
            @RequestParam(name = "name", required = false) Optional<String> plantName,
            @RequestParam(name = "startDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Optional<LocalDate> startDate,
            @RequestParam(name = "endDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Optional<LocalDate> endDate) {
        if (plantName.isPresent() && startDate.isPresent() && endDate.isPresent()) {
            return rentalService.findAvailablePlants(plantName.get(), startDate.get(), endDate.get());
        } else {
            return rentalService.findAllAvailablePlants();
        }
    }

    @GetMapping("/orders")
    public List<PurchaseOrderDTO> findSubmittedPurchaseOrders() {
        return rentalService.findSubmittedPurchaseOrders();
    }

    @GetMapping("/orders/{id}")
    public PurchaseOrderDTO findPurchaseOrder(@PathVariable("id") String id) {
        return rentalService.findPurchaseOrder(id);
    }

    @PostMapping("/orders")
    public ResponseEntity createPurchaseOrder(@RequestBody PlantHireRequestDTO hireRequestDTO) {
        return rentalService.createPurchaseOrder(hireRequestDTO);
    }

    @PutMapping("/orders")
    public ResponseEntity resubmitPurchaseOrder(@RequestBody PurchaseOrderDTO purchaseOrderDTO) {
        return rentalService.resubmitPurchaseOrder(purchaseOrderDTO);
    }

    @DeleteMapping("/orders/{id}")
    public ResponseEntity cancelPurchaseOrder(@PathVariable("id") String purchaseOrderId) {
        //TODO: Implement
        //procurementService.cancelPlantHireRequest(purchaseOrderId);
        return rentalService.cancelPurchaseOrder(purchaseOrderId);
    }

}

@JsonSerialize
class EmptyJsonResponse { }

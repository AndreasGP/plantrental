package ee.esi.team8.procurement.repo;

        import ee.esi.team8.procurement.domain.model.PHRStatus;
        import ee.esi.team8.procurement.domain.model.PlantHireRequest;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.data.domain.Example;
        import org.springframework.data.domain.Page;
        import org.springframework.data.domain.Pageable;
        import org.springframework.data.domain.Sort;
        import org.springframework.data.jpa.repository.JpaRepository;
        import org.springframework.data.jpa.repository.Query;
        import org.springframework.stereotype.Repository;

        import javax.persistence.EntityManager;
        import java.util.List;

/**
 * Repository for Plant Hire Request
 * (First appearance: hw6)
 */

public class PlantHireRequestRepositoryImpl implements CustomPlantHireRequestRepository {

    @Autowired
    EntityManager em;

    public PlantHireRequest findByPurchaseOrderId(String purchaseOrderId) {
        System.out.println("PO id: " + purchaseOrderId);
        return em.createQuery("select phr from PlantHireRequest phr where phr.purchaseOrder.href like :poid", PlantHireRequest.class)
                .setParameter("poid", "%" + purchaseOrderId).getSingleResult();
    }
}

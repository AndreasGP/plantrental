package ee.esi.team8.procurement.repo;

import ee.esi.team8.procurement.domain.model.PHRStatus;
import ee.esi.team8.procurement.domain.model.PlantHireRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Repository for Plant Hire Request
 * (First appearance: hw6)
 */
public interface CustomPlantHireRequestRepository {

    PlantHireRequest findByPurchaseOrderId(String purchaseOrderId);
}

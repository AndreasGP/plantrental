package ee.esi.team8.procurement.repo;

import ee.esi.team8.procurement.domain.model.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository for Comment
 * (First appearance: hw4: milestone 2)
 */
@Repository
public interface CommentRepository extends JpaRepository<Comment, String> {
}